# After Grafana installed, add prometheus datasource
configuration > datasource > add datasource 
type : prometheus  
name : prometheus  
adress : http://localhost:9090  

# Add node exporter dashboard
Dashboard > manage > import  
Id : 11074 (node-exporter)  
Id : 10467 (windows-exporter)  
Id : 13105 (kube)  